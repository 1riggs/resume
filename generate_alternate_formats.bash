#!/usr/bin/env bash
set -e

echo "copying changes from resume.md to README.md"
cp -i resume.md README.md

echo "creating html document using style.css"
pandoc -o "$PWD"/html/resume.html --ascii --css "$PWD"/html/style.css "$PWD"/resume.md &&
    sed -i '1s/^/<head><link href="style.css" rel="stylesheet"><\/link><\/head>\n/' "$PWD"/html/resume.html

echo "creating pdf document based on html document"
wkhtmltopdf --enable-local-file-access "$PWD"/html/resume.html "$PWD"/pdf/resume.pdf

# scp html/resume.html alex@192.168.0.61:/home/alex/drshevek.com/static/resume.html
