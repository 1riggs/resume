Alexander Riggs
======

#### Problem solving enthusiast with a strong background in math and programming

###### [ [GitLab](https://gitlab.com/1riggs) ] . [ [Github](https://github.com/1riggs) ] . [ [LinkedIn](https://www.linkedin.com/in/alexander-riggs-647528157) ] . [ ajriggs29@gmail.com ] . [ 262 349 6884 ]

Skills
---------
**Programming:** Python, SQL, git, Javascript, Java, MATLAB

**Python Tools**: scikit-learn, pandas, NumPy, SciPy, SQLAlchemy, NetworkX, FastAPI

**Database Systems:** PostgreSQL, Elasticsearch, Oracle APEX, SQLite

**GIS Tools:** GDAL/OGR, QGIS, PostGIS, GeoServer, tippecanoe, TiTiler, Nominatim

**Linux & Utilities:** Debian & RHEL based distros, bash, vim, sed, awk, cron

**Server Administration & DevOps:** AWS, Docker, Jenkins, Terraform, Kubernetes, Helm, NGINX

**AWS:** S3, Elasticsearch Service, EC2, ECR, RDS, Cloudfront, Route53, EKS

**Javascript:** Node, Express, React, TypeScript

Experience
---------
**Software Engineer, Outside Analytics Inc.**  (2018-present, Boulder CO)

* Managed and designed database schemas for multiple projects
* Contributed to APIs which utilized NodeJS with ExpressJS, and Sequelize
* Architected and administered CI/CD infrastructure using Docker, Terraform, Jenkins, Chef, AWS EKS, Kubernetes, and Helm
* Processed large GIS data sets and created map display assets
* Contributed to a statistical model using pandas, NumPy, & SciPy of hunt license lottery in multiple states
* Contributed to a machine learning model which predicts a hunter's chance of success in a geographic area
* Mentored junior developers through code reviews and peer programming
* Created git guidelines to unify and streamline the companies version control strategy

**Undergraduate Researcher, University of Wisconsin Milwaukee** (2017-2018, Milwaukee WI)

* Co-author of [Scaling Behavior of Drug Transport and Absorption in In Silico Cerebral Capillary Network](https://journals.plos.org/plosone/article?id=10.1371/journal.pone.0200266)

Education
---------

**B. S. in Applied Mathematics and Computer Science, University of Wisconsin - Milwaukee** (2013-2018)

* Undergraduate research on mathematical modeling of biological systems
* Coursework in linear algebra, optimzation, mathematical modeling, and algorithms

Projects
--------
**Personal Website**

* Simple website for tinkering and self-hosting projects
* Uses NGINX, docker & docker-compose, and letsencrypt
* Hosted on a Linode Cloud Server

**Web-accessible Security Camera**

* Webcam turned security camera which allows a logged in user to take a picture, and records video based on motion activity
* Uses MotionEye, docker & docker-compose
* Hosted on a Raspberry Pi4

**DWM API**

* A utility application which leverages graph theory to optimize decision making in a video game
* Utilizes pandas, FastAPI, PostgreSQL, SQLAchemy, Alembic, and Docker
